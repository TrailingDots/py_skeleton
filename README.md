# Py_Skeleton: How to structure a Python project

Inspired by https://github.com/kennethreitz/samplemod.

Further inspired by  https://github.com/bast/somepackage.git .

Arguably I have attempted to create a skeleton with the
best practices for python structures.

# Installation

`py_skeleton` lives as a single file in your `$HOME/bin`
directory. `py_skeleton` may be placed in any other directory
in your path.

Clone from gitlab and copy the `py_skeleton`:

~~~ bash
    git clone https://gitlab.com/TrailingDots/py_skeleton.git
    cd py_skeleton
    cp py_skeleton $HOME/bin
    chmod a+x $HOME/bin/py_skeleton
~~~

# Using py_skeleton

To start some new project such as `awesome_project`, cd
to the location for development.


~~~ bash
    cd some_dir     # Your new project base directory
    py_skeleton awesome_project
    cd awesome_project
    vim conda.sh    # Adjust to proper conda environment
    make tests      # Basic tests with pytest
~~~

You may need to install `pytest` for your conda environment.

## Skeleton Created

The generated files as displayed by `tree .`:

~~~ text
    /usr/bin/tree .
    .
    ├── awesome_project
    │   ├── awesome_project.py
    │   ├── data
    │   ├── __init__.py
    │   ├── __pycache__
    │   ├── run_tests.py
    │   └── tests
    │       ├── __init__.py
    │       ├── __pycache__
    │       └── test_awesome_project.py
    ├── backup-git.sh
    ├── backup.sh
    ├── conda.sh
    ├── docs
    │   ├── conf.py
    │   ├── index.rst
    │   └── quickstart.rst
    ├── Makefile
    ├── README.md
    ├── run_tests.py
    ├── setup.cfg
    ├── setup.py
    └── wc.sh
~~~

Notice that tests as under the source code directory and *not*
in a parallel directory. This makes it easier to move to
another project if necessary. This solution to tests management
is arguable.

The `Makefile` groups multiple commands useful for project
management together in a single place.

## conda.sh

Because each project is assumed to operate under an
anaconda3 shell, the 'conda.sh' associated with each
project provides a record of which environment
the project uses. Developers do not have to guess
or remember which environment to use.

Some larger projects use the project name as the
environment name. This is perfectly fine.

Change the last line of conda.sh to the appropriate
anaconda3 environment and use as:

~~~ bash
    . ./conda.sh
~~~

By dotting the conda.sh, the contents of this file
get integrated into the current shell and not a subshell
as with normal script activation.

The contents of conda.sh:

~~~ bash
    #
    # Do NOT use this as a shell!
    # MUST DOT this file as:
    #     . conda.sh
    #
    # DOT this file to activate the correct
    # virtual anaconda3 environment
    #
    # To see all virtual envs:
    #     conda info -e
    #
    echo Change the anaconda3 environment to your
    echo project environment!

    conda activate py38   # <- Change py38 to YOUR env!
~~~

Other than  changing the conda environment, nothing is
required of this file.

If you have never used dot files and need further information: [Getting started with
Dotfiles](https://medium.com/@webprolific/getting-started-with-dotfiles-43c3602fd789)

## Makefile

The Makefile contains all the directives to access the
supporting utilities in one central location.

Makefiles provide freedom for developers as they do not have
to remember specific commands for each phase of project
develoment. Developers have plenty to remember as they
write, debug and enhance code and `Makefile` applies a
centralized location to run many steps.

A simple `make help` generally suffices to
direct help for a project development step.

Not every utility will satisfy
every developer. Please feel free to remove unnecessary
or unwanted utilities.

`make help` lists all targets:

~~~ text
    make help     - This help message.
    make build    - Build source distributable package. Test locally
    make coverage - Run test and coverage suite.
    make tests    - Run test suite.
    make install  - install on local system
    make backup   - Create tgz backup one dir above this dir.
    make wc       - Perform word count for line counts.
    make clean    - Get rid of scratch files
    make doc      - Create the html file from README.md
    make lint     - Perform extensive lint with prospector and radon
    make linters  - Run pyflakes, flake8, pylint, pycodestyle.
~~~

Every developer should use "make tests", "make coverage",
"make linters" as they develop and test.

Unfortunately the exact commands for such items as "make install"
and "make build" are not possible for this skeleton
and are left as an exercise for each project.
Other targets may require modification for a project.

### Makefile targets/utilities

Developers may not have all utilities in their environment.

To install a utility, either `pip` or `conda` will install them.
Use whichever install works:

~~~ bash
    pip install coverage    # Try pip or ...
    conda install coverage  # ... try conda if pip fails
~~~

Either install results in a successful installation.

The utilities used in this Makefile:

~~~ text
    coverage    # Code coverage
    pytest      # Python testing
    radon       # Complexity, maintainibility, etc.
    prospector  # Errors, problems, violations and complexity
    pyflakes    # Checks source files for errors
    flake8      # Wrapper for PyFlakes, pycodestyle and mccabe
~~~

Yes, this list contains utilities that overlap, so
feel free to adjust for your needs.

# Great resources

- https://manikos.github.io/a-tour-on-python-packaging
- https://docs.python-guide.org/writing/structure/
- http://veekaybee.github.io/2017/09/26/python-packaging/
- https://github.com/audreyr/cookiecutter-pypackage
- https://medium.com/@jonas.r.kemper/understanding-best-practice-python-tooling-by-comparing-popular-project-templates-6eba49229106


# Recommendations

Please browse this blog post for a good summary of recommendations 
and templates: https://medium.com/@jonas.r.kemper/understanding-best-practice-python-tooling-by-comparing-popular-project-templates-6eba49229106


# Extensions to SomePackage

I liked SomePackage (see reference at the beginning), but
I wanted to extend it.

Specifically I wanted:

- A better, faster and more centralized method of performing
  common tasks such a linting, coverage, etc.
  My solution was to use a standard linux Makefile.
- A way to set the anaconda3 environment for a specific project.
  While venv presents a very fine virtual environment, I
  don't use it. Why? I'm happy with anaconda3's conda.
  Please add your own environment tracking script.
- A word counting shell for general interest.
- Run various code diagnostic and code style utilities.
- Run coverage as for developing code and tests.
  Coverage and tests while developing offer best practices
  for any modern development.
- A standard way of running all the tests and utilities.
  'make help' presents a simple way to start most operations
  and eliminates the necessity to remember infrequently
  used commands.
- Remove scratch file that can accumulate in testing and dev.
- Create documentation and installation scripts.

# Suggestions? Corrections? Pull requests?

Yes please!
